#include "password.h"
#include "ui_password.h"
#include <QMessageBox>

using namespace boost::filesystem;
using namespace std;

password::password(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::password)
{
    ui->setupUi(this);
}

password::~password()
{
    delete ui;
}

void password::on_pushButton_clicked()
{
    key ="# " + ui->lineEdit->text().toStdString();
    char buff[50]; // буфер промежуточного хранения считываемого из файла текста
    ifstream fin("/home/nikolay/QtProjects/FirstProject/lockbox/passwords"); // открыли файл для чтения

try{
    bool find=false;
    while (!fin.eof()){ //пока не конец файла
            fin.getline(buff, 50);
            if (key == buff) {
                find = true;
                fin >> buff;
                while ((*buff != '#') && (!fin.eof())){
                    emit FileName(buff);
                    fin >> buff;
                }
            }
        }
     fin.close();
     if (!find)
         throw "uncorrect password";
     else
         this->close();
   }
   catch(char const*  message){
      QMessageBox::warning(this,"very strange...", message);
   }
}

void password::on_AddUser_clicked()
{
    ofstream fout("/home/nikolay/QtProjects/FirstProject/lockbox/passwords", ios::app);
    fout << "\n" << "# " << ui->lineEdit_2->text().toStdString();
    fout.close(); // закрываем файл
    this->close();
}
