#ifndef PASSWORD_H
#define PASSWORD_H

#include <QDialog>
#include <string>
#include <iostream>
#include <fstream>
#include <boost/filesystem.hpp>

namespace Ui {
class password;
}

class password : public QDialog
{
    Q_OBJECT

public:
    explicit password(QWidget *parent = 0);
    ~password();

private slots:
    void on_pushButton_clicked();

    void on_AddUser_clicked();
signals:
    void FileName(char*);

private:
    Ui::password *ui;
    std::string key;
};

#endif // PASSWORD_H
